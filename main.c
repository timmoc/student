#include <stdio.h>
#include <stdlib.h>
#include <mem.h>

typedef struct {
    char name[31];
    char id[6];
    char phone[12];
} Student;
void removestdchar(char array[]) {
    if (strchr(array, '\n') == NULL) {
        while (fgetc(stdin) != '\n');
    }
}

void menu() {
    printf("MENU:\n");
    printf("1. add a student\n");
    printf("2. open the current list\n");
    printf("3. save file\n");
    printf("4. open file\n");
    printf("5. exit\n");
    printf("choose here(1|2|3|4|5)\n");
}

    int main() {
        FILE *fp;
        int digits = 0;
        int repeat = 0;
        Student student[10];
        while (1) {
            int choice;
            menu();
            scanf("%d", &choice);
            getchar();
            switch (choice) {
                case 1:
                    while (digits == 0) {
                        printf("Enter your student's ID:(5 digits).\n");
                        fgets(student[repeat].id, 6, stdin);
                        removestdchar(student[repeat].id);
                        if (strlen(student[repeat].id) != 5) {
                            printf("the ID needs to have 5 digits\n");
                        } else {
                            digits = 1;
                        }
                    }
                    printf("Enter your student's name:");
                    fgets(student[repeat].name, 31, stdin);
                    removestdchar(student[repeat].name);
                    printf("Enter your student's phone:");
                    fgets(student[repeat].phone, 12, stdin);
                    removestdchar(student[repeat].phone);
                    printf("your  student's name:%s\n", student[repeat].name);
                    printf("your  student's ID:%s\n", student[repeat].id);
                    printf("your  student's phone:%s\n", student[repeat].phone);
                    int choiceoflist;
                    printf("do you want to save ?(1.save|2.don't save).");
                    scanf("%d", &choiceoflist);
                    if (choiceoflist == 1) {
                        if (repeat > 10) {
                            printf("full");
                        } else {
                            ++repeat;
                        }

                        break;
                        case 2:
                            printf("|%15s|%15s|%15s|\n", "student ID", "name", "phone number");
                        for (int i = 0; i < repeat; ++i) {
                            printf("|%15s|%15s|%15s|\n", student[i].id, student[i].name, student[i].phone);
                        }
                        break;
                        case 3:
                            fp = fopen("list.txt", "w");
                            fprintf(fp, "|%15s|%15s|%15s|\n", "student ID", "name", "phone number");
                            for (int i = 0; i < repeat; ++i) {
                                fprintf(fp, "|%15s|%15s|%15s|\n", student[i].id, student[i].name, student[i].phone);
                            }
                            fclose(fp);


                            break;
                            case 4:
                                fp = fopen("list.txt", "r");
                            fscanf(fp,"|%15s|%15s|%15s|\n", "student ID", "name", "phone number");
                            for (int i = 0; i < repeat; ++i) {
                                fscanf(fp,"|%15s|%15s|%15s|\n", student[i].id, student[i].name, student[i].phone);
                            }
                            fclose(fp);
                            break;
                            case 5:
                                exit(1);
                            default:
                                printf("WRONG.");
                            break;
                        }
                    }
            }
        }

